<?php

namespace iThemes\Lib\CLITools;

use League\CLImate\CLImate;

interface CLImateFactory
{
    /**
     * Make a CLImate instance.
     *
     * @return CLImate
     */
    public function make(): CLImate;
}
