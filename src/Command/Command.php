<?php

namespace iThemes\Lib\CLITools\Command;

use League\CLImate\CLImate;

interface Command
{
    /**
     * Execute the command.
     *
     * @param CLImate $climate
     *
     * @return int Exit code. 0 for success, >0 for error.
     */
    public function __invoke(CLImate $climate): int;

    /**
     * Get the command name.
     *
     * @example generate-posts
     *
     * @return string
     */
    public static function getName(): string;

    /**
     * Get the user-facing description of this command.
     *
     * @return string
     */
    public static function getDescription(): string;

    /**
     * Get the list of supported arguments. Formatted for {@see \League\CLImate\Argument\Manager::add()}
     *
     * @return array
     */
    public static function getArguments(): array;
}
