<?php

namespace iThemes\Lib\CLITools\Command;

interface HasSubcommands
{
    /**
     * Get the subcommands attached to this command.
     *
     * @return string[]|Command[] List of class names.
     */
    public static function getSubcommands(): array;
}
