<?php

namespace iThemes\Lib\CLITools;

use League\CLImate\TerminalObject\Dynamic\DynamicTerminalObject;

class Editor extends DynamicTerminalObject
{
    /** @var string */
    private $input;

    /** @var string */
    private $title;

    /**
     * Editor constructor.
     *
     * @param string $input
     * @param string $title
     */
    public function __construct(string $input = '', string $title = 'CLI')
    {
        $this->input = $input;
        $this->title = $title;
    }

    public function prompt(): string
    {
        return self::launchEditor($this->input, $this->title);
    }

    /**
     * Launch the editor for input.
     *
     * From WP-CLI.
     *
     * @param string $input Existing input to edit.
     * @param string $title Window title.
     *
     * @return string
     * @throws \RuntimeException
     */
    private static function launchEditor(string $input, string $title): string
    {
        $tmpdir = sys_get_temp_dir();

        if (! endsWith($tmpdir, '/')) {
            $tmpdir .= '/';
        }

        $fileName = basename($title);
        $fileName = preg_replace('|\.[^.]*$|', '', $fileName);
        $fileName .= '-' . substr(md5(mt_rand()), 0, 6);

        $filePath = $tmpdir . $fileName . '.tmp';

        $fp = fopen($filePath, 'xb');

        if (! $fp) {
            throw new \RuntimeException('Error creating temporary file');
        }

        if ($fp) {
            fclose($fp);
        }

        file_put_contents($filePath, $input);

        $editor = getenv('EDITOR') ?: 'vi';

        $descriptorSpec = [ STDIN, STDOUT, STDERR ];

        $process = proc_open("$editor " . escapeshellarg($filePath), $descriptorSpec, $pipes);

        if ($r = proc_close($process)) {
            throw new \RuntimeException(sprintf('Failed to open editor. Exited with code %d', $r));
        }

        $output = file_get_contents($filePath);

        unlink($filePath);

        return $output;
    }
}
