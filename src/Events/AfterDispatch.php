<?php

namespace iThemes\Lib\CLITools\Events;

use iThemes\Lib\CLITools\Command\Command;
use League\CLImate\CLImate;

final class AfterDispatch
{
    /** @var Command */
    private $command;

    /** @var CLImate */
    private $climate;

    /** @var int */
    private $exitCode;

    /**
     * BeforeDispatch constructor.
     *
     * @param Command $command
     * @param CLImate $climate
     * @param int     $exitCode
     */
    public function __construct(Command $command, CLImate $climate, int $exitCode)
    {
        $this->command = $command;
        $this->climate = $climate;
        $this->exitCode = $exitCode;
    }

    /**
     * Get the command about to be executed.
     *
     * @return Command
     */
    public function getCommand(): Command
    {
        return $this->command;
    }

    /**
     * Get the configured climate instance that will be passed to the Command.
     *
     * @return CLImate
     */
    public function getClimate(): CLImate
    {
        return $this->climate;
    }

    /**
     * Get the exit code.
     *
     * @return int
     */
    public function getExitCode(): int
    {
        return $this->exitCode;
    }
}
