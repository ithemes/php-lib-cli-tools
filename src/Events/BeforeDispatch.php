<?php

namespace iThemes\Lib\CLITools\Events;

use iThemes\Lib\CLITools\Command\Command;
use League\CLImate\CLImate;

final class BeforeDispatch
{
    /** @var Command */
    private $command;

    /** @var CLImate */
    private $climate;

    /**
     * BeforeDispatch constructor.
     *
     * @param Command $command
     * @param CLImate $climate
     */
    public function __construct(Command $command, CLImate $climate)
    {
        $this->command = $command;
        $this->climate = $climate;
    }

    /**
     * Get the command about to be executed.
     *
     * @return Command
     */
    public function getCommand(): Command
    {
        return $this->command;
    }

    /**
     * Get the configured climate instance that will be passed to the Command.
     *
     * @return CLImate
     */
    public function getClimate(): CLImate
    {
        return $this->climate;
    }
}
