<?php

namespace iThemes\Lib\CLITools;

use iThemes\Lib\CLITools\Command\Command;

final class Match
{
    /** @var Command */
    private $command;

    /** @var string */
    private $path;

    /** @var array */
    private $args;

    public function __construct(Command $command, string $path, array $args)
    {
        $this->command = $command;
        $this->path = $path;
        $this->args = $args;
    }

    public function getCommand(): Command
    {
        return $this->command;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getArgs(): array
    {
        return $this->args;
    }
}
