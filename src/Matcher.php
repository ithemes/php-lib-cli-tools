<?php

namespace iThemes\Lib\CLITools;

use iThemes\Lib\CLITools\Command\Command;
use iThemes\Lib\CLITools\Command\HasSubcommands;
use Psr\Container\ContainerInterface;

class Matcher
{
    /** @var ContainerInterface */
    private $container;

    /**
     * This is a list of classnames. The object typehint is for IDE purposes.
     *
     * @var string[]|Command[]
     */
    private $commands;

    /**
     * CommandMatcher constructor.
     *
     * @param ContainerInterface $container
     * @param array              $commands
     */
    public function __construct(ContainerInterface $container, array $commands)
    {
        $this->container = $container;
        $this->commands  = $commands;
    }

    /**
     * Find and instantiate the command object for the given command name.
     *
     * @param array $argv Must include the program name as the 0th argument.
     *
     * @return Match|null
     */
    public function find(array $argv): ?Match
    {
        $allowed = $this->commands;
        $command = null;
        /** @var Command|HasSubcommands $command */
        $arg     = next($argv);
        $i       = 0;
        $cmdPath = '';

        while ($arg && ! startsWith($arg, '-')) {
            $i++;
            $cmdPath .= ' ' . $arg;

            if (! $command = $this->findCommandIn($arg, $allowed)) {
                return null;
            }

            if (! is_subclass_of($command, HasSubcommands::class)) {
                $args    = array_slice($argv, $i + 1);
                $cmdPath = trim($cmdPath);

                return new Match(
                    $this->container->get($command),
                    $cmdPath,
                    $args
                );
            }

            $arg     = next($argv);
            $allowed = $command::getSubcommands();
        }

        if ($command) {
            $args    = array_slice($argv, $i + 1);
            $cmdPath = trim($cmdPath);

            return new Match(
                $this->container->get($command),
                $cmdPath,
                $args
            );
        }

        return null;
    }

    /**
     * Find the command in a set of available commands.
     *
     * @param string             $name
     * @param string[]|Command[] $classes
     *
     * @return string|null
     */
    private function findCommandIn(string $name, array $classes): ?string
    {
        foreach ($classes as $class) {
            if ($class::getName() === $name) {
                return $class;
            }
        }

        return null;
    }
}
