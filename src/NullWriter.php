<?php

namespace iThemes\Lib\CLITools;

use League\CLImate\Util\Writer\WriterInterface;

class NullWriter implements WriterInterface
{
    public function write($content)
    {
    }
}
