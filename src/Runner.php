<?php

namespace iThemes\Lib\CLITools;

use iThemes\Lib\CLITools\Command\Command;
use iThemes\Lib\CLITools\Command\HasSubcommands;
use iThemes\Lib\CLITools\Events\AfterDispatch;
use iThemes\Lib\CLITools\Events\BeforeDispatch;
use League\CLImate\Argument\Argument;
use League\CLImate\Argument\Filter;
use League\CLImate\Argument\Summary;
use League\CLImate\CLImate;
use League\CLImate\Exceptions\InvalidArgumentException;
use Psr\EventDispatcher\EventDispatcherInterface;

class Runner
{
    private const GLOBAL = [
        'help'  => [
            'prefix'      => 'H',
            'longPrefix'  => 'help',
            'noValue'     => true,
            'description' => 'Displays this screen',
        ],
        'quiet' => [
            'prefix'      => 'Q',
            'longPrefix'  => 'quiet',
            'noValue'     => true,
            'description' => 'Do not display any output. Only exit with a status code.',
        ],
        'debug' => [
            'prefix'      => 'D',
            'longPrefix'  => 'debug',
            'noValue'     => true,
            'description' => 'Output debugging statements.',
        ],
    ];

    /** @var Matcher */
    private $matcher;

    /** @var CLImateFactory */
    private $climateFactory;

    /** @var EventDispatcherInterface|null */
    private $eventDispatcher;

    /**
     * This is a list of classnames. The object typehint is for IDE purposes.
     *
     * @var string[]|Command[]
     */
    private $commands;

    /**
     * Runner constructor.
     *
     * @param Matcher        $matcher
     * @param CLImateFactory $climateFactory
     * @param string[]       $commands List of command classnames.
     */
    public function __construct(Matcher $matcher, CLImateFactory $climateFactory, array $commands)
    {
        $this->matcher        = $matcher;
        $this->climateFactory = $climateFactory;
        $this->commands       = $commands;

        array_walk($commands, static function (string $class, $i) {
            if (! is_subclass_of($class, Command::class)) {
                throw new \InvalidArgumentException(sprintf('$commands[%s] does not implement Command', $i));
            }
        });
    }

    /**
     * Set the event dispatcher instance.
     *
     * @param EventDispatcherInterface|null $eventDispatcher
     *
     * @return $this
     */
    public function setEventDispatcher(?EventDispatcherInterface $eventDispatcher): self
    {
        $this->eventDispatcher = $eventDispatcher;

        return $this;
    }

    /**
     * Run the CLI session.
     *
     * @param array $argv CLI arguments. Must include the program name as the 0th argument.
     *
     * @return int The exit code. 0 for success, > 0 for error.
     */
    public function run(array $argv): int
    {
        $climate = $this->climateFactory->make();

        if (count($argv) < 2) {
            $this->printCommands($climate);

            return 0;
        }

        if (! $match = $this->matcher->find($argv)) {
            if (
                ! in_array('-' . self::GLOBAL['quiet']['prefix'], $argv, true) &&
                ! in_array('--' . self::GLOBAL['quiet']['longPrefix'], $argv, true)
            ) {
                $climate->to('error')->error('Command not found.');
            }

            return 1;
        }

        $command = $match->getCommand();

        $args = array_merge([ $command::getName() ], $match->getArgs());
        $path = $match->getPath();

        $climate->description($command::getDescription());
        $climate->arguments->add($command::getArguments());
        $climate->arguments->add(self::GLOBAL);

        if ($climate->arguments->defined('help', $args)) {
            $this->usage($climate, $command, $path);

            return 0;
        }

        if ($climate->arguments->defined('quiet', $args)) {
            foreach ($climate->output->getAvailable() as $key => $writer) {
                $climate->output->add($key, new NullWriter());
            }

            $climate->output->defaultTo('out');
        }

        try {
            $climate->arguments->parse($args);
        } catch (InvalidArgumentException $e) {
            $climate->to('error')->error($e->getMessage());

            return 1;
        }

        if ($this->eventDispatcher) {
            $this->eventDispatcher->dispatch(new BeforeDispatch($command, $climate));
        }

        $exitCode = $command($climate);

        if ($this->eventDispatcher) {
            $this->eventDispatcher->dispatch(new AfterDispatch($command, $climate, $exitCode));
        }

        return $exitCode;
    }

    /**
     * Print the usage for a command.
     *
     * This includes the description, command path list of arguments and sub commands.
     *
     * @param CLImate $climate
     * @param Command $command
     * @param string  $cmdPath
     */
    private function usage(CLImate $climate, Command $command, string $cmdPath): void
    {
        (new Summary())
            ->setClimate($climate)
            ->setDescription($command::getDescription())
            ->setCommand('bin/cli' . ' ' . $cmdPath)
            ->setFilter(
                new Filter(),
                arrayWithoutKeys($climate->arguments->all(), array_keys(self::GLOBAL))
            )
            ->output();

        $summary = (new Summary())
            ->setClimate($climate);

        if ($command instanceof HasSubcommands && $subcommands = $command::getSubcommands()) {
            $climate->br()->out('Additional Commands:');

            foreach ($subcommands as $subcommand) {
                $climate->tab()->out(
                    'bin/cli' . ' ' .
                    $cmdPath . ' ' .
                    $subcommand::getName() . ' ' .
                    $summary
                        ->setClimate($climate)
                        ->short(mapCombine($subcommand::getArguments(), static function ($params, $name) {
                            return [ $name, Argument::createFromArray($name, $params) ];
                        }))
                );
            }
        }

        $arguments = $climate->arguments->all();

        $climate->br()->out('Global Arguments:');

        foreach (self::GLOBAL as $name => $config) {
            $argument = $arguments[ $name ];
            $climate->tab()->out($summary->argument($argument));

            if ($argument->description()) {
                $climate->tab(2)->out($argument->description());
            }
        }
    }

    /**
     * Print the list of available commands.
     *
     * @param CLImate $climate
     */
    private function printCommands(CLImate $climate): void
    {
        $lengths = map($this->commands, static function ($command) {
            return strlen($command::getName());
        });
        $padding = $climate->padding(max($lengths) + 5, ' ');

        foreach ($this->commands as $command) {
            $padding->label($command::getName())->result($command::getDescription());
        }
    }
}
