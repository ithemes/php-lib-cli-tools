<?php
declare(strict_types=1);

namespace iThemes\Lib\CLITools;

use function strlen;

/**
 * Check if the given string ends with the given needle.
 *
 * @param string $haystack
 * @param string $needle
 *
 * @return bool
 */
function endsWith(string $haystack, string $needle): bool
{
    return substr($haystack, -strlen($needle)) === $needle;
}

/**
 * Check if the given string starts with the given needle.
 *
 * @param string $haystack
 * @param string $needle
 *
 * @return bool
 */
function startsWith(string $haystack, string $needle): bool
{
    return strpos($haystack, $needle) === 0;
}

/**
 * Apply a callback over an iterable list.
 *
 * @param iterable $iterable
 * @param callable $callback
 *
 * @return array
 */
function map(iterable $iterable, callable $callback): array
{
    $mapped = [];

    foreach ($iterable as $key => $item) {
        $mapped[ $key ] = $callback($item, $key);
    }

    return $mapped;
}

/**
 * Build an array of keys and values by mapping over a set of values.
 *
 * @param iterable $iterable
 * @param callable $callback Return a tuple of key, value.
 *
 * @return array
 */
function mapCombine(iterable $iterable, callable $callback): array
{
    $arr = [];

    foreach ($iterable as $k => $item) {
        [ $key, $value ] = $callback($item, $k);

        $arr[ $key ] = $value;
    }

    return $arr;
}

/**
 * Generate an array without the given fields.
 *
 * @param array $array
 * @param array $fields
 *
 * @return array
 */
function arrayWithoutKeys(array $array, array $fields): array
{
    $picked = [];
    $flip   = array_flip($fields);

    foreach ($array as $key => $value) {
        if (! isset($flip[ $key ])) {
            $picked[ $key ] = $value;
        }
    }

    return $picked;
}
