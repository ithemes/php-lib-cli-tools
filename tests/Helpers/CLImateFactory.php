<?php

namespace iThemes\Lib\CLITools\Tests\Helpers;

use League\CLImate\CLImate;

class CLImateFactory implements \iThemes\Lib\CLITools\CLImateFactory
{
    /** @var CLImate */
    private $climate;

    /**
     * CLImateFactory constructor.
     *
     * @param CLImate $climate
     */
    public function __construct(CLImate $climate = null)
    {
        $this->climate = $climate ?? new CLImate();
    }

    public function make(): CLImate
    {
        return $this->climate;
    }
}
