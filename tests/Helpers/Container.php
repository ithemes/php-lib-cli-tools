<?php

namespace iThemes\Lib\CLITools\Tests\Helpers;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class Container implements ContainerInterface
{
    /** @var array */
    private $entries;

    /**
     * Container constructor.
     *
     * @param array $entries
     */
    public function __construct(array $entries)
    {
        $this->entries = $entries;
    }

    public function get($id)
    {
        return $this->entries[ $id ];
    }

    public function has($id)
    {
        return isset($this->entries[ $id ]);
    }
}
