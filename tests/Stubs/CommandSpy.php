<?php

namespace iThemes\Lib\CLITools\Tests\Stubs;

use iThemes\Lib\CLITools\Command\Command;
use League\CLImate\CLImate;

class CommandSpy implements Command
{
    public static $name = 'spy';
    public static $description = 'My Cool Spy.';
    public static $arguments = [];

    public $invocations = [];

    /** @var callable */
    private $callable;

    /**
     * CommandSpy constructor.
     *
     * @param callable $callable
     */
    public function __construct(callable $callable = null)
    {
        $this->callable = $callable;
    }

    public function __invoke(CLImate $climate): int
    {
        $this->invocations[] = $climate;

        return $this->callable ? call_user_func($this->callable, $climate) : 0;
    }

    public static function getName(): string
    {
        return static::$name;
    }

    public static function getDescription(): string
    {
        return static::$description;
    }

    public static function getArguments(): array
    {
        return static::$arguments;
    }

    public static function reset(): void
    {
        static::$name        = 'spy';
        static::$description = 'My Cool Spy.';
        static::$arguments   = [];
    }
}
