<?php

namespace iThemes\Lib\CLITools\Tests\Suites\Unit;

use iThemes\Lib\CLITools\{Command\Command, Command\HasSubcommands, Matcher};
use League\CLImate\CLImate;
use Psr\Container\ContainerInterface;
use \PHPUnit\Framework\TestCase;

class CommandMatcherTest extends TestCase
{

    public function testMatchReturnsNullIfNoCommandsAvailable(): void
    {
        $matcher = new Matcher(
            $this->getMockBuilder(ContainerInterface::class)->setMethods([ 'get', 'has' ])->getMock(),
            []
        );
        self::assertNull($matcher->find([ 'bin/cli' ]));
    }

    public function testMatchTopLevelLeafCommand(): void
    {
        $cmd = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'cmd';
            }

            public static function getDescription(): string
            {
                return 'Command';
            }

            public static function getArguments(): array
            {
                return [];
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($cmd))
            ->willReturn($cmd);

        $match = (new Matcher($mock, [ get_class($cmd) ]))->find([ 'bin/cli', $cmd::getName() ]);

        self::assertNotNull($match);
        self::assertSame($cmd, $match->getCommand());
        self::assertEquals([], $match->getArgs());
        self::assertEquals($cmd::getName(), $match->getPath());
    }

    public function testMatchTopLevelLeafCommandWithPositionalArg(): void
    {
        $cmd = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'cmd';
            }

            public static function getDescription(): string
            {
                return 'Command';
            }

            public static function getArguments(): array
            {
                return [
                    'arg1' => []
                ];
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($cmd))
            ->willReturn($cmd);

        $match = (new Matcher($mock, [ get_class($cmd) ]))->find([ 'bin/cli', $cmd::getName(), 'val1' ]);

        self::assertNotNull($match);
        self::assertSame($cmd, $match->getCommand());
        self::assertEquals([ 'val1' ], $match->getArgs());
        self::assertEquals($cmd::getName(), $match->getPath());
    }

    public function testMatchTopLevelLeafCommandWithLongPrefixArg(): void
    {
        $cmd = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'cmd';
            }

            public static function getDescription(): string
            {
                return 'Command';
            }

            public static function getArguments(): array
            {
                return [
                    'arg1' => [
                        'longPrefix' => 'arg1',
                    ]
                ];
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($cmd))
            ->willReturn($cmd);

        $match = (new Matcher($mock, [ get_class($cmd) ]))->find([ 'bin/cli', $cmd::getName(), '--arg1', 'val1' ]);

        self::assertNotNull($match);
        self::assertSame($cmd, $match->getCommand());
        self::assertEquals([ '--arg1', 'val1' ], $match->getArgs());
        self::assertEquals($cmd::getName(), $match->getPath());
    }

    public function testMatchTopLevelLeafCommandWithLongPrefixNoValueArg(): void
    {
        $cmd = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'cmd';
            }

            public static function getDescription(): string
            {
                return 'Command';
            }

            public static function getArguments(): array
            {
                return [
                    'arg1' => [
                        'noValue'    => true,
                        'longPrefix' => 'arg1',
                    ]
                ];
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($cmd))
            ->willReturn($cmd);

        $match = (new Matcher($mock, [ get_class($cmd) ]))->find([ 'bin/cli', $cmd::getName(), '--arg1' ]);

        self::assertNotNull($match);
        self::assertSame($cmd, $match->getCommand());
        self::assertEquals([ '--arg1' ], $match->getArgs());
        self::assertEquals($cmd::getName(), $match->getPath());
    }

    public function testMatchTopLevelLeafCommandWithShortPrefixArg(): void
    {
        $cmd = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'cmd';
            }

            public static function getDescription(): string
            {
                return 'Command';
            }

            public static function getArguments(): array
            {
                return [
                    'arg1' => [
                        'shortPrefix' => 'A',
                    ]
                ];
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($cmd))
            ->willReturn($cmd);

        $match = (new Matcher($mock, [ get_class($cmd) ]))->find([ 'bin/cli', $cmd::getName(), '-A', 'val1' ]);

        self::assertNotNull($match);
        self::assertSame($cmd, $match->getCommand());
        self::assertEquals([ '-A', 'val1' ], $match->getArgs());
        self::assertEquals($cmd::getName(), $match->getPath());
    }

    public function testMatchTopLevelLeafCommandWithShortPrefixNoValueArg(): void
    {
        $cmd = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'cmd';
            }

            public static function getDescription(): string
            {
                return 'Command';
            }

            public static function getArguments(): array
            {
                return [
                    'arg1' => [
                        'noValue'     => true,
                        'shortPrefix' => 'A',
                    ]
                ];
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($cmd))
            ->willReturn($cmd);

        $match = (new Matcher($mock, [ get_class($cmd) ]))->find([ 'bin/cli', $cmd::getName(), '-A' ]);

        self::assertNotNull($match);
        self::assertSame($cmd, $match->getCommand());
        self::assertEquals([ '-A' ], $match->getArgs());
        self::assertEquals($cmd::getName(), $match->getPath());
    }

    public function testMatchSubLeafCommand(): void
    {
        $leaf = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'leaf';
            }

            public static function getDescription(): string
            {
                return 'Leaf Command';
            }

            public static function getArguments(): array
            {
                return [];
            }
        };

        $root = new class implements Command, HasSubcommands
        {
            public static $sub = [];

            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'root';
            }

            public static function getDescription(): string
            {
                return 'Root Command';
            }

            public static function getArguments(): array
            {
                return [];
            }

            public static function getSubcommands(): array
            {
                return static::$sub;
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($leaf))
            ->willReturn($leaf);

        $root::$sub = [ get_class($leaf) ];

        $match = (new Matcher($mock, [ get_class($root) ]))->find([ 'bin/cli', $root::getName(), $leaf::getName() ]);

        self::assertNotNull($match);
        self::assertSame($leaf, $match->getCommand());
        self::assertEquals([], $match->getArgs());
        self::assertEquals("{$root::getName()} {$leaf::getName()}", $match->getPath());
    }

    public function testMatchSubLeafCommandWithPositionalArg(): void
    {
        $leaf = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'leaf';
            }

            public static function getDescription(): string
            {
                return 'Leaf Command';
            }

            public static function getArguments(): array
            {
                return [ 'arg1' => [] ];
            }
        };

        $root = new class implements Command, HasSubcommands
        {
            public static $sub = [];

            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'root';
            }

            public static function getDescription(): string
            {
                return 'Root Command';
            }

            public static function getArguments(): array
            {
                return [];
            }

            public static function getSubcommands(): array
            {
                return static::$sub;
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($leaf))
            ->willReturn($leaf);

        $root::$sub = [ get_class($leaf) ];

        $match = (new Matcher($mock, [ get_class($root) ]))
            ->find([ 'bin/cli', $root::getName(), $leaf::getName(), 'arg1' ]);

        self::assertNotNull($match);
        self::assertSame($leaf, $match->getCommand());
        self::assertEquals([ 'arg1' ], $match->getArgs());
        self::assertEquals("{$root::getName()} {$leaf::getName()}", $match->getPath());
    }

    public function testMatchSubLeafCommandWithLongPrefixArg(): void
    {
        $leaf = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'leaf';
            }

            public static function getDescription(): string
            {
                return 'Leaf Command';
            }

            public static function getArguments(): array
            {
                return [ 'arg1' => [ 'longPrefix' => 'arg1' ] ];
            }
        };

        $root = new class implements Command, HasSubcommands
        {
            public static $sub = [];

            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'root';
            }

            public static function getDescription(): string
            {
                return 'Root Command';
            }

            public static function getArguments(): array
            {
                return [];
            }

            public static function getSubcommands(): array
            {
                return static::$sub;
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($leaf))
            ->willReturn($leaf);

        $root::$sub = [ get_class($leaf) ];

        $match = (new Matcher($mock, [ get_class($root) ]))
            ->find([ 'bin/cli', $root::getName(), $leaf::getName(), '--arg1', 'val1' ]);

        self::assertNotNull($match);
        self::assertSame($leaf, $match->getCommand());
        self::assertEquals([ '--arg1', 'val1' ], $match->getArgs());
        self::assertEquals("{$root::getName()} {$leaf::getName()}", $match->getPath());
    }

    public function testMatchSubLeafCommandWithShortPrefixArg(): void
    {
        $leaf = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'leaf';
            }

            public static function getDescription(): string
            {
                return 'Leaf Command';
            }

            public static function getArguments(): array
            {
                return [ 'arg1' => [ 'shortPrefix' => 'A' ] ];
            }
        };

        $root = new class implements Command, HasSubcommands
        {
            public static $sub = [];

            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'root';
            }

            public static function getDescription(): string
            {
                return 'Root Command';
            }

            public static function getArguments(): array
            {
                return [];
            }

            public static function getSubcommands(): array
            {
                return static::$sub;
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($leaf))
            ->willReturn($leaf);

        $root::$sub = [ get_class($leaf) ];

        $match = (new Matcher($mock, [ get_class($root) ]))
            ->find([ 'bin/cli', $root::getName(), $leaf::getName(), '-A', 'val1' ]);

        self::assertNotNull($match);
        self::assertSame($leaf, $match->getCommand());
        self::assertEquals([ '-A', 'val1' ], $match->getArgs());
        self::assertEquals("{$root::getName()} {$leaf::getName()}", $match->getPath());
    }

    public function testMatchRootCommandWithLeafs(): void
    {
        $leaf = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'leaf';
            }

            public static function getDescription(): string
            {
                return 'Leaf Command';
            }

            public static function getArguments(): array
            {
                return [];
            }
        };

        $root = new class implements Command, HasSubcommands
        {
            public static $sub = [];

            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'root';
            }

            public static function getDescription(): string
            {
                return 'Root Command';
            }

            public static function getArguments(): array
            {
                return [];
            }

            public static function getSubcommands(): array
            {
                return static::$sub;
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($root))
            ->willReturn($root);

        $root::$sub = [ get_class($leaf) ];

        $match = (new Matcher($mock, [ get_class($root) ]))->find([ 'bin/cli', $root::getName() ]);

        self::assertNotNull($match);
        self::assertSame($root, $match->getCommand());
        self::assertEquals([], $match->getArgs());
        self::assertEquals($root::getName(), $match->getPath());
    }

    public function testMatchRootCommandWithLeafsAndLongPrefixArg(): void
    {
        $leaf = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'leaf';
            }

            public static function getDescription(): string
            {
                return 'Leaf Command';
            }

            public static function getArguments(): array
            {
                return [];
            }
        };

        $root = new class implements Command, HasSubcommands
        {
            public static $sub = [];

            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'root';
            }

            public static function getDescription(): string
            {
                return 'Root Command';
            }

            public static function getArguments(): array
            {
                return [ 'arg1' => [ 'longPrefix' => 'arg1' ] ];
            }

            public static function getSubcommands(): array
            {
                return static::$sub;
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($root))
            ->willReturn($root);

        $root::$sub = [ get_class($leaf) ];

        $match = (new Matcher($mock, [ get_class($root) ]))
            ->find([ 'bin/cli', $root::getName(), '--arg1', 'val1' ]);

        self::assertNotNull($match);
        self::assertSame($root, $match->getCommand());
        self::assertEquals([ '--arg1', 'val1' ], $match->getArgs());
        self::assertEquals($root::getName(), $match->getPath());
    }

    public function testMatchRootCommandWithLeafsAndShortPrefixArg(): void
    {
        $leaf = new class implements Command
        {
            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'leaf';
            }

            public static function getDescription(): string
            {
                return 'Leaf Command';
            }

            public static function getArguments(): array
            {
                return [ 'arg1' => [ 'shortPrefix' => 'A' ] ];
            }
        };

        $root = new class implements Command, HasSubcommands
        {
            public static $sub = [];

            public function __invoke(CLImate $climate): int
            {
                return 0;
            }

            public static function getName(): string
            {
                return 'root';
            }

            public static function getDescription(): string
            {
                return 'Root Command';
            }

            public static function getArguments(): array
            {
                return [];
            }

            public static function getSubcommands(): array
            {
                return static::$sub;
            }
        };

        $mock = $this->getContainerMock();

        $mock->expects($this->atLeastOnce())
            ->method('get')
            ->with(get_class($root))
            ->willReturn($root);

        $root::$sub = [ get_class($leaf) ];

        $match = (new Matcher($mock, [ get_class($root) ]))
            ->find([ 'bin/cli', $root::getName(), '-A', 'val1' ]);

        self::assertNotNull($match);
        self::assertSame($root, $match->getCommand());
        self::assertEquals([ '-A', 'val1' ], $match->getArgs());
        self::assertEquals($root::getName(), $match->getPath());
    }

    protected function getContainerMock()
    {
        $mock = $this
            ->getMockBuilder(ContainerInterface::class)
            ->setMethods([ 'get', 'has' ])
            ->getMock();

        $mock->method('has')->willReturn(true);

        return $mock;
    }
}
