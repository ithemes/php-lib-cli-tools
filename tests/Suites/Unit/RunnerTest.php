<?php

namespace iThemes\Lib\CLITools\Tests\Suites\Unit;

use iThemes\Lib\CLITools\Command\Command;
use iThemes\Lib\CLITools\Matcher;
use iThemes\Lib\CLITools\Runner;
use iThemes\Lib\CLITools\Tests\Helpers\CLImateFactory;
use iThemes\Lib\CLITools\Tests\Helpers\Container;
use iThemes\Lib\CLITools\Tests\Stubs\CommandSpy;
use League\CLImate\CLImate;
use League\CLImate\Util\Writer\Buffer;
use PHPUnit\Framework\TestCase;

class RunnerTest extends TestCase
{
    private $outWriter;
    private $errorWriter;
    private $bufferWritter;

    protected function setUp(): void
    {
        parent::setUp();

        $this->outWriter     = new Buffer();
        $this->errorWriter   = new Buffer();
        $this->bufferWritter = new Buffer();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        CommandSpy::reset();
    }

    public function testPrintsSummaryWhenNoCommandPassed(): void
    {
        $SUT = $this->getRunner([ CommandSpy::class ]);
        self::assertEquals(0, $SUT->run([ 'bin/cli' ]), $this->errorWriter->get());

        self::assertRegExp('/spy.+My Cool Spy/', $this->outWriter->get());
    }

    public function testPrintsUsageWhenHelpFlagPassed(): void
    {
        $SUT = $this->getRunner([ CommandSpy::class ]);
        self::assertEquals(0, $SUT->run([ 'bin/cli', 'spy', '--help' ]), $this->errorWriter->get());

        self::assertStringContainsString('Usage: bin/cli spy', $this->outWriter->get());
    }

    public function testQuiet(): void
    {
        $SUT = $this->getRunner([
            new CommandSpy(static function (CLImate $climate) {
                $climate->out('Success!');
                $climate->to('error')->out('Error!');

                return 0;
            }),
        ]);

        self::assertEquals(0, $SUT->run([ 'bin/cli', 'spy', '-Q' ]));
        self::assertEquals('', $this->outWriter->get());
        self::assertEquals('', $this->errorWriter->get());
    }

    public function testArgumentsPassed(): void
    {
        CommandSpy::$arguments = [
            'myArg' => [
                'longPrefix' => 'myArg',
            ],
        ];

        $SUT = $this->getRunner([
            new CommandSpy(static function (CLImate $climate) {
                static::assertEquals('hi', $climate->arguments->get('myArg'));

                return 0;
            }),
        ]);

        self::assertEquals(0, $SUT->run([ 'bin/cli', 'spy', '--myArg', 'hi' ]));
    }

    public function testPrintsArgumentErrorToStdErr(): void
    {
        CommandSpy::$arguments = [
            'myArg' => [
                'longPrefix' => 'myArg',
                'required'   => true,
            ]
        ];

        $SUT = $this->getRunner([ CommandSpy::class ]);
        self::assertEquals(1, $SUT->run([ 'bin/cli', 'spy' ]), $this->errorWriter->get());
        self::assertStringContainsString('required', $this->errorWriter->get());
        self::assertStringContainsString('--myArg', $this->errorWriter->get());
    }

    private function getRunner($commands): Runner
    {
        $forContainer = [];

        foreach ($commands as $classOrCommand) {
            if (is_string($classOrCommand)) {
                $forContainer[ $classOrCommand ] = new $classOrCommand();
            } else {
                $forContainer[ get_class($classOrCommand) ] = $classOrCommand;
            }
        }

        $container = new Container($forContainer);

        $climate = new CLImate();
        $climate->output->add('out', $this->outWriter);
        $climate->output->add('error', $this->errorWriter);
        $climate->output->add('buffer', $this->bufferWritter);
        $climate->output->defaultTo('out');

        return new Runner(
            new Matcher($container, array_keys($forContainer)),
            new CLImateFactory($climate),
            array_keys($forContainer)
        );
    }
}
